ActiveAdmin.register Appointment do
  permit_params :appointmentdate, :order, :booked, user_attributes: [:id, :username]

  form do |f|
    f.inputs do
      f.input :order
      f.input :appointmentdate, :as => :datetime_picker
      f.input :booked
    end
    f.actions

  end

  index  do
  column :appointmentdate
  column :booked
  column("Order", :sortable => :id) {|order| link_to "##{order.id} ", admin_order_path(order) }
    actions
  end




end

